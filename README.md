# Design Patterns OOP with Java
## _Singleton_

This project implements a simplified example for design pattern  Singleton. 

The singleton pattern consists of implementing a class that creates a global instance for the application. 
This single instance will be used by all that depend they instance.

## Example

A database contains limited connections available.
A good idea to optimize resource usage is to create a pool of connections that will be used and then returned to the pool without closing the connection, saving the time it would take to open and close each one.

```java
class ConnectionPool{
    private static final ConnectionPool INSTANCE = new ConnectionPool(); // create a instance static to global use

    public static ConnectionPool getInstance() {
        return INSTANCE; // permites use all instance methods and variables 
    }
}
```

```java
class Client{
    public static findAll() {
        ConnectionPool.getInstance().query("SELECT * FROM PEOPLES"); // This use one connections of pool
    }
}
```

```java
class Client2{
    public static findAll() {
        ConnectionPool.getInstance().query("SELECT * FROM CLIENTS"); // call same instance of Client
    }
}
```



