package com.jonasrodrigues.database;

public enum Status {
    STAND_BY, CONNECTED, CLOSED, CREATED
}
